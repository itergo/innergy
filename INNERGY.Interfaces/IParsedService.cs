﻿using INNERGY.Models;
using System;
using System.Collections.Generic;

namespace INNERGY.Interfaces {
    public interface IParsedService {
        public List<Warehouse> Warehouses { get; set; }
        /// <param name="line"></param>
        /// <exception cref="ValidationException">Throwed when line isn't correct format</exception>
        void ParseLine(string line);
    }
}
