﻿using INNERGY.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace INNERGY.Interfaces {
    public interface IFormatService {
        string PrintWarehouseSummary(List<Warehouse> warehouses);
    }
}
