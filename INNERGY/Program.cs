﻿using INNERGY.Interfaces;
using INNERGY.Services;
using System;
using System.ComponentModel.DataAnnotations;

namespace INNERGY {
    class Program {
        static void Main(string[] args) {
            string line;

            IParsedService parsedService = new ParsedService();
            IFormatService formatService = new FormatService();

            Console.WriteLine("Welcome to text converter for INNERGY by Michal Grontkowski.");
            Console.WriteLine("To get result please write q and accept by enter");

            while ((line = Console.ReadLine()) != null) {
                if (line == "q") {
                    break;
                }
                try {
                    parsedService.ParseLine(line);
                } catch (ValidationException e) {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message);
                    Console.ResetColor();

                }
            }
            Console.Write(formatService.PrintWarehouseSummary(parsedService.Warehouses));

        }
    }
}
