﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INNERGY.Models {
    public class ParsedLine {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public string WarehouseName { get; set; }
        public int Quantity { get; set; }
        public string[] WarehousesWithQuantity { get; set; }
    }
}
