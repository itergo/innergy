﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INNERGY.Models {
    public class Warehouse {
        public string Name { get; set; }
        public int Total { get; set; }
        public List<WarehouseProduct> WarehouseProducts { get; set; }

        public Warehouse() {
            WarehouseProducts = new List<WarehouseProduct>();
        }
    }
}
