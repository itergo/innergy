﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INNERGY.Models {
    public class WarehouseProduct {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public int Quantity { get; set; }
    }
}
