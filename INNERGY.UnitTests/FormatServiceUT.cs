﻿using INNERGY.Interfaces;
using INNERGY.Models;
using INNERGY.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace INNERGY.UnitTests {
    public class FormatServiceUT {
        public IFormatService formatService { get; set; }
        [SetUp]
        public void Setup() {
            formatService = new FormatService();
        }

        [Test]
        public void CheckPrintWarehouseSummary_WH_A() {
            var wha = new Warehouse() {
                Name = "WH-A",
                Total = 50,
                WarehouseProducts = new List<WarehouseProduct>() {
                    new WarehouseProduct() {
                        MaterialId = "3M-Cherry-10mm",
                        Quantity = 10
                    },
                      new WarehouseProduct() {
                        MaterialId = "COM-100001",
                        Quantity = 5
                    },
                        new WarehouseProduct() {
                        MaterialId = "COM-123906c",
                        Quantity = 10
                    },
                          new WarehouseProduct() {
                        MaterialId = "COM-123908",
                        Quantity = 10
                    },
                            new WarehouseProduct() {
                        MaterialId = "COM-124047",
                        Quantity = 15
                    }
                }
            };

            string result = formatService.PrintWarehouseSummary(new List<Models.Warehouse>(){
                wha });

            Assert.AreEqual(
            $"WH-A (total 50)" +
            $"{Environment.NewLine}3M-Cherry-10mm: 10" +
            $"{Environment.NewLine}COM-100001: 5" +
            $"{Environment.NewLine}COM-123906c: 10" +
            $"{Environment.NewLine}COM-123908: 10" +
            $"{Environment.NewLine}COM-124047: 15", result);
        }
        [Test]
        public void CheckPrintWarehouseSummary_WH_B() {
            var whb = new Warehouse() {
                Name = "WH-B",
                Total = 33,
                WarehouseProducts = new List<WarehouseProduct>() {
                    new WarehouseProduct() {
                        MaterialId = "3M-Cherry-10mm",
                        Quantity = 1
                    },
                    new WarehouseProduct() {
                        MaterialId = "CB0115-CASSRC",
                        Quantity = 5
                    },
                    new WarehouseProduct() {
                        MaterialId = "COM-100001",
                        Quantity = 10
                    },
                    new WarehouseProduct() {
                        MaterialId = "COM-123906c",
                        Quantity = 6
                    },
                    new WarehouseProduct() {
                        MaterialId = "COM-123908",
                        Quantity = 11
                    }
                }
            };

            string result = formatService.PrintWarehouseSummary(new List<Models.Warehouse>(){
                whb });

            Assert.AreEqual(
            $"WH-B (total 33)" +
            $"{Environment.NewLine}3M-Cherry-10mm: 1" +
            $"{Environment.NewLine}CB0115-CASSRC: 5" +
            $"{Environment.NewLine}COM-100001: 10" +
            $"{Environment.NewLine}COM-123906c: 6" +
            $"{Environment.NewLine}COM-123908: 11", result);
        }
        [Test]
        public void CheckPrintWarehouseSummary_WH_C() {
            var whc = new Warehouse() {
                Name = "WH-C",
                Total = 33,
                WarehouseProducts = new List<WarehouseProduct>() {
                    new WarehouseProduct() {
                        MaterialId = "CB0115-CASSRC",
                        Quantity = 13
                    },
                      new WarehouseProduct() {
                        MaterialId = "COM-101734",
                        Quantity = 8
                    },
                        new WarehouseProduct() {
                        MaterialId = "COM-123823",
                        Quantity = 10
                    },
                          new WarehouseProduct() {
                        MaterialId = "COM-123906c",
                        Quantity = 2
                    }
                }
            };

            string result = formatService.PrintWarehouseSummary(new List<Models.Warehouse>(){
                whc });

            Assert.AreEqual(
            $"WH-C (total 33)" +
            $"{Environment.NewLine}CB0115-CASSRC: 13" +
            $"{Environment.NewLine}COM-101734: 8" +
            $"{Environment.NewLine}COM-123823: 10" +
            $"{Environment.NewLine}COM-123906c: 2", result);
        }

        [Test]
        public void CheckPrintWarehouseSummaryTotal() {
            var wha = new Warehouse() {
                Name = "WH-A",
                Total = 50,
                WarehouseProducts = new List<WarehouseProduct>() {
                    new WarehouseProduct() {
                        MaterialId = "3M-Cherry-10mm",
                        Quantity = 10
                    },
                      new WarehouseProduct() {
                        MaterialId = "COM-100001",
                        Quantity = 5
                    },
                        new WarehouseProduct() {
                        MaterialId = "COM-123906c",
                        Quantity = 10
                    },
                          new WarehouseProduct() {
                        MaterialId = "COM-123908",
                        Quantity = 10
                    },
                            new WarehouseProduct() {
                        MaterialId = "COM-124047",
                        Quantity = 15
                    }
                }
            };

            var whb = new Warehouse() {
                Name = "WH-B",
                Total = 33,
                WarehouseProducts = new List<WarehouseProduct>() {
                    new WarehouseProduct() {
                        MaterialId = "3M-Cherry-10mm",
                        Quantity = 1
                    },
                    new WarehouseProduct() {
                        MaterialId = "CB0115-CASSRC",
                        Quantity = 5
                    },
                    new WarehouseProduct() {
                        MaterialId = "COM-100001",
                        Quantity = 10
                    },
                    new WarehouseProduct() {
                        MaterialId = "COM-123906c",
                        Quantity = 6
                    },
                    new WarehouseProduct() {
                        MaterialId = "COM-123908",
                        Quantity = 11
                    }
                }
            };

            var whc = new Warehouse() {
                Name = "WH-C",
                Total = 33,
                WarehouseProducts = new List<WarehouseProduct>() {
                    new WarehouseProduct() {
                        MaterialId = "CB0115-CASSRC",
                        Quantity = 13
                    },
                      new WarehouseProduct() {
                        MaterialId = "COM-101734",
                        Quantity = 8
                    },
                        new WarehouseProduct() {
                        MaterialId = "COM-123823",
                        Quantity = 10
                    },
                          new WarehouseProduct() {
                        MaterialId = "COM-123906c",
                        Quantity = 2
                    }
                }
            };

            string result = formatService.PrintWarehouseSummary(new List<Models.Warehouse>(){
                whb, wha,whc });

            Assert.AreEqual(
                $"WH-A (total 50)" +
                $"{Environment.NewLine}3M-Cherry-10mm: 10" +
                $"{Environment.NewLine}COM-100001: 5" +
                $"{Environment.NewLine}COM-123906c: 10" +
                $"{Environment.NewLine}COM-123908: 10" +
                $"{Environment.NewLine}COM-124047: 15" +
                $"{Environment.NewLine}" +
                $"{Environment.NewLine}WH-C (total 33)" +
                $"{Environment.NewLine}CB0115-CASSRC: 13" +
                $"{Environment.NewLine}COM-101734: 8" +
                $"{Environment.NewLine}COM-123823: 10" +
                $"{Environment.NewLine}COM-123906c: 2" +
                $"{Environment.NewLine}" +
                $"{Environment.NewLine}WH-B (total 33)" +
                $"{Environment.NewLine}3M-Cherry-10mm: 1" +
                $"{Environment.NewLine}CB0115-CASSRC: 5" +
                $"{Environment.NewLine}COM-100001: 10" +
                $"{Environment.NewLine}COM-123906c: 6" +
                $"{Environment.NewLine}COM-123908: 11", result);
        }
    }
}
