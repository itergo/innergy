using INNERGY.Interfaces;
using INNERGY.Services;
using NUnit.Framework;
using System.Linq;

namespace INNERGY.UnitTests {
    public class ParsedServiceUT {
        [SetUp]
        public void Setup() {
        }

        [Test]
        public void IgnoreLineStartedWithHash() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("# Material inventory initial state as of Jan 01 2018");

            Assert.AreEqual(0, parsedService.Warehouses.Count);
        }
        [Test]
        public void SaveLineWithoutHash() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");

            Assert.AreEqual(3, parsedService.Warehouses.Count);
        }
        [Test]
        public void Format_1_level_of_COM_100001() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");

            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-A");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-100001");
            Assert.AreEqual(warehouseProduct.MaterialName, "Cherry Hardwood Arched Door - PS");
            Assert.AreEqual(warehouseProduct.Quantity, 10);
        }
        [Test]
        public void Format_2_level_of_COM_100001() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");

            var warehouse = parsedService.Warehouses[1];
            Assert.AreEqual(warehouse.Name, "WH-B");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-100001");
            Assert.AreEqual(warehouseProduct.MaterialName, "Cherry Hardwood Arched Door - PS");
            Assert.AreEqual(warehouseProduct.Quantity, 5);
        }

        [Test]
        public void Format_1_level_of_COM_124047() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Maple Dovetail Drawerbox;COM-124047;WH-A,15");

            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-A");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialName, "Maple Dovetail Drawerbox");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-124047");
            Assert.AreEqual(warehouseProduct.Quantity, 15);
        }
        [Test]
        public void Format_1_levef_of_COM_123906c() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");


            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-A");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialName, "Generic Wire Pull");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-123906c");
            Assert.AreEqual(warehouseProduct.Quantity, 2);
        }

        [Test]
        public void Format_2_levef_of_COM_123906c() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");

            var warehouse = parsedService.Warehouses[1];
            Assert.AreEqual(warehouse.Name, "WH-B");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialName, "Generic Wire Pull");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-123906c");
            Assert.AreEqual(warehouseProduct.Quantity, 10);
        }
        [Test]
        public void Format_3_levef_of_COM_123906c() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");

            var warehouse = parsedService.Warehouses[2];
            Assert.AreEqual(warehouse.Name, "WH-C");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialName, "Generic Wire Pull");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-123906c");
            Assert.AreEqual(warehouseProduct.Quantity, 6);
        }

        [Test]
        public void Format_1_level_of_COM_123908() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");

            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-A");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];

            Assert.AreEqual(warehouseProduct.MaterialName, "Yankee Hardware 110 Deg. Hinge");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-123908");
            Assert.AreEqual(warehouseProduct.Quantity, 11);
        }

        [Test]
        public void Format_2_level_of_COM_123908() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");

            var warehouse = parsedService.Warehouses[1];
            Assert.AreEqual(warehouse.Name, "WH-B");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];

            Assert.AreEqual(warehouseProduct.MaterialName, "Yankee Hardware 110 Deg. Hinge");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-123908");
            Assert.AreEqual(warehouseProduct.Quantity, 10);
        }

        [Test]
        public void Format_1_level_of_3MCherry_10mm() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1");


            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-A");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];

            Assert.AreEqual(warehouseProduct.MaterialName, "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back");
            Assert.AreEqual(warehouseProduct.MaterialId, "3MCherry-10mm");
            Assert.AreEqual(warehouseProduct.Quantity, 1);
        }

        [Test]
        public void Format_2_level_of_3MCherry_10mm() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1");


            var warehouse = parsedService.Warehouses[1];
            Assert.AreEqual(warehouse.Name, "WH-B");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];

            Assert.AreEqual(warehouseProduct.MaterialName, "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back");
            Assert.AreEqual(warehouseProduct.MaterialId, "3MCherry-10mm");
            Assert.AreEqual(warehouseProduct.Quantity, 10);
        }

        [Test]
        public void Format_1_level_of_COM_123823() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10");

            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-C");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialName, "Veneer - Cherry Rotary 1 FSC");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-123823");
            Assert.AreEqual(warehouseProduct.Quantity, 10);
        }
        [Test]
        public void Format_1_level_of_COM_101734() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("MDF, CARB2, 1 1/8\";COM-101734;WH-C,8 ");

            var warehouse = parsedService.Warehouses[0];
            Assert.AreEqual(warehouse.Name, "WH-C");
            Assert.AreEqual(warehouse.WarehouseProducts.Count, 1);

            var warehouseProduct = warehouse.WarehouseProducts[0];
            Assert.AreEqual(warehouseProduct.MaterialName, "MDF, CARB2, 1 1/8\"");
            Assert.AreEqual(warehouseProduct.MaterialId, "COM-101734");
            Assert.AreEqual(warehouseProduct.Quantity, 8);
        }

        [Test]
        public void CheckWarehouseCount() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");
            parsedService.ParseLine("Maple Dovetail Drawerbox;COM-124047;WH-A,15");
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");
            parsedService.ParseLine("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");
            parsedService.ParseLine("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1");
            parsedService.ParseLine("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10");
            parsedService.ParseLine("MDF, CARB2, 1 1/8\";COM-101734;WH-C,8 ");

            Assert.AreEqual(parsedService.Warehouses.Count, 3);
        }
        [Test]
        public void Check_WH_A_Total() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");
            parsedService.ParseLine("Maple Dovetail Drawerbox;COM-124047;WH-A,15");
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");
            parsedService.ParseLine("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");
            parsedService.ParseLine("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1");
            parsedService.ParseLine("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10");
            parsedService.ParseLine("MDF, CARB2, 1 1/8\";COM-101734;WH-C,8 ");

            var warehouse = parsedService.Warehouses.FirstOrDefault(x => x.Name == "WH-A");

            Assert.AreEqual(39, warehouse.Total);
        }
        [Test]
        public void Check_WH_B_Total() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");
            parsedService.ParseLine("Maple Dovetail Drawerbox;COM-124047;WH-A,15");
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");
            parsedService.ParseLine("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");
            parsedService.ParseLine("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1");
            parsedService.ParseLine("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10");
            parsedService.ParseLine("MDF, CARB2, 1 1/8\";COM-101734;WH-C,8 ");

            var warehouse = parsedService.Warehouses.FirstOrDefault(x => x.Name == "WH-B");

            Assert.AreEqual(35, warehouse.Total);
        }
        [Test]
        public void Check_WH_C_Total() {
            IParsedService parsedService = new ParsedService();
            parsedService.ParseLine("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");
            parsedService.ParseLine("Maple Dovetail Drawerbox;COM-124047;WH-A,15");
            parsedService.ParseLine("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");
            parsedService.ParseLine("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");
            parsedService.ParseLine("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1");
            parsedService.ParseLine("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10");
            parsedService.ParseLine("MDF, CARB2, 1 1/8\";COM-101734;WH-C,8 ");

            var warehouse = parsedService.Warehouses.FirstOrDefault(x => x.Name == "WH-C");

            Assert.AreEqual(24, warehouse.Total);
        }

    }
}