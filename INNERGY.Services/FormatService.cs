﻿using INNERGY.Interfaces;
using INNERGY.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INNERGY.Services {
    public class FormatService : IFormatService {
        public string PrintWarehouseSummary(List<Warehouse> warehouses) {

            var sorted = warehouses.OrderByDescending(x => x.Total).ThenByDescending(x=>x.Name);
            string result = "";

            foreach (var item in sorted.Select((warehouse, i) => new { i, warehouse })) {

                if (item.i != 0) {
                    result += Environment.NewLine;
                    result += Environment.NewLine;
                }

                result += $"{item.warehouse.Name} (total {item.warehouse.Total})";

                foreach (var product in item.warehouse.WarehouseProducts) {
                    result += Environment.NewLine;
                    result += $"{product.MaterialId}: {product.Quantity}";
                }

            }
            return result;
        }
    }
}
