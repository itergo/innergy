﻿using INNERGY.Interfaces;
using INNERGY.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace INNERGY.Services {

    public class ParsedService : IParsedService {
        public List<Warehouse> Warehouses { get; set; }
        private Regex _fullRegex = new Regex(@"^(?<MaterialName>.*?);(?<MaterialID>.*);(?<WarehouseName>.*?),(?<WarehouseWithQuantity>.*),(?<Quantity>[\w\.]+)$", RegexOptions.Compiled);
        private Regex _partRegex = new Regex(@"^(?<MaterialName>.*?);(?<MaterialID>.*);(?<WarehouseName>.*?),(?<Quantity>[\w\.]+)$", RegexOptions.Compiled);

        public ParsedService() {
            Warehouses = new List<Warehouse>();
        }
        /// <param name="line"></param>
        /// <exception cref="ValidationException">Throwed when line isn't correct format</exception>
        public void ParseLine(string line) {
            if (String.IsNullOrWhiteSpace(line) || line[0] == '#') {
                return;
            }
            var parsedLine = Parse(line.Trim());

            AddProductToWarehouse(parsedLine.WarehouseName, parsedLine.MaterialName, parsedLine.MaterialId, parsedLine.Quantity);

            for (int i = 0; i < parsedLine.WarehousesWithQuantity.Length; i++) {
                //5|WH-B
                var warehousesWithQuantity = parsedLine.WarehousesWithQuantity[i].Split('|');

                AddProductToWarehouse(warehousesWithQuantity[1], parsedLine.MaterialName, parsedLine.MaterialId, int.Parse(warehousesWithQuantity[0]));
            }
        }
        private ParsedLine Parse(string line) {
            var matched = _fullRegex.Match(line);

            if (matched.Success == false) {

                matched = _partRegex.Match(line);
                
                if(matched.Success == false) {
                    throw new ValidationException("Line isn't correct format");
                }
            }

            var quantity = string.IsNullOrWhiteSpace(matched.Groups["Quantity"].Value) ? 0 : int.Parse(matched.Groups["Quantity"].Value);

            return new ParsedLine() {
                MaterialName = matched.Groups["MaterialName"].Value,
                MaterialId = matched.Groups["MaterialID"].Value,
                WarehouseName = matched.Groups["WarehouseName"].Value,
                WarehousesWithQuantity = matched.Groups["WarehouseWithQuantity"].Value.Split(',', StringSplitOptions.RemoveEmptyEntries),
                Quantity = quantity
            };
        }

        private Warehouse AddProductToWarehouse(string warehouseName, string materialName, string materialId, int quantity) {
            var warehouse = Warehouses.FirstOrDefault(x => x.Name == warehouseName);

            if (warehouse == null) {
                warehouse = AddWarehouse(warehouseName);
            }

            warehouse.Total += quantity;
            warehouse.WarehouseProducts.Add(new WarehouseProduct() {
                MaterialName = materialName,
                MaterialId = materialId,
                Quantity = quantity
            }); ;

            return warehouse;
        }
        private Warehouse AddWarehouse(string warehouseName) {
            var warehouse = new Warehouse() {
                Name = warehouseName
            };

            Warehouses.Add(warehouse);

            return warehouse;
        }
    }
}
